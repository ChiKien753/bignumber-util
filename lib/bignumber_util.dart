library bignumber_util;

String sum(String no1, String no2) {
  int num1, num2, tmp = 0, sum;
  String res = '';

  if (no1.length > no2.length) {
    while(no2.length < no1.length) {
      no2 = '0' + no2;
    }
  } else if (no1.length < no2.length){
    while(no2.length > no1.length) {
      no1 = '0' + no1;
    }
  }
  for (int i = no1.length - 1; i >= 0; i--) {
    num1 = int.parse(no1[i]);
    num2 = int.parse(no2[i]);
    sum = num1 + num2 + tmp;
    if (sum >= 10) {
      tmp = 1;
      res = (sum % 10).toString() + res; 
    } else {
      res = sum.toString() + res; 
    }
  }
  return res;
}
